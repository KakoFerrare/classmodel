﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassModel.Model
{
    public class ContaCorrente
    {
        public string IdConta { get; set; }
        public int IdPac { get; set; }
        public int TipoConta { get; set; }
        public int IdStatus { get; set; }
        public int IdPerfil { get; set; }
        public string IdCpf1 { get; set; }
        public string IdCpf2 { get; set; }
        public string IdCpf3 { get; set; }
        public string IdCpf4 { get; set; }
        //public Endereco End_Correspondencia { get; set; }
        public int End_Correspond { get; set; }
        public int Tit_Correspond { get; set; }
        public string Nome1_Cheque { get; set; }
        public int Tit_Cheque1 { get; set; }
        public string Nome2_Cheque { get; set; }
        public int Tit_Cheque2 { get; set; }
        public int Qtd_Ch10 { get; set; }
        public int Tipo_Ch10 { get; set; }
        public int Qtd_Ch20 { get; set; }
        public int Tipo_Ch20 { get; set; }
        public int Qtd_Canhoto { get; set; }
        public int Tipo_Canhoto { get; set; }
        public int Qtd_SemCanhoto { get; set; }
        public int Tipo_SemCanhoto { get; set; }
        public int UltimoCheque { get; set; }
        public string RestricaoTalao { get; set; }
        public DateTime DtAbertura { get; set; }
        public DateTime DtBloqueio { get; set; }
        public DateTime DtEncerramento { get; set; }
        public DateTime DtTarifa { get; set; }
        public double Tarifa { get; set; }
        public string Senha { get; set; }
        public string SenhaWeb { get; set; }
        public string IdMatricula { get; set; }
        public string Mat_Cap { get; set; }
        public string Bonus { get; set; }
        public int IdUsuario_Inc { get; set; }
        public DateTime Dt_Inc { get; set; }
        public int IdUsuario_Alt { get; set; }
        public DateTime Dt_Alt { get; set; }

    }
}
