﻿using ClassModel.Model;
using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ClassModel.DAO
{
    public class DAOContaCorrente
    {
        private StringBuilder strQuery = new StringBuilder();
        private string table = "ContaCorrente";
        private FbConnection conn = new FbConnection();
        public ContaCorrente Carregar(string id, string stringConnection)
        {
            strQuery = new StringBuilder();
            ContaCorrente Conta = new ContaCorrente();
            strQuery.Append(" Select * from " + table + " WHERE IDCONTA = @IdConta");
            using (conn = new FbConnection(stringConnection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.Add("@IdConta", id);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                Conta = ConstructObject(Conta, reader);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Conta = new ContaCorrente();
                }
            }
            return Conta;
        }

        public List<ContaCorrente> Listar(string stringConnection)
        {
            strQuery = new StringBuilder();
            List<ContaCorrente> listaContaCorrent = new List<ContaCorrente>();
            strQuery.Append(" Select * from " + table);
            using (conn = new FbConnection(stringConnection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                listaContaCorrent.Add(ConstructObject(new ContaCorrente(), reader));
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    listaContaCorrent = new List<ContaCorrente>();
                }
            }
            return listaContaCorrent;
        }

        private ContaCorrente ConstructObject(ContaCorrente conta, DbDataReader reader)
        {
            foreach (PropertyInfo propertyInfo in conta.GetType().GetProperties())
            {
                if (propertyInfo.Name != "Error")
                    propertyInfo.SetValue(conta, reader[propertyInfo.Name] == DBNull.Value ? null : reader[propertyInfo.Name], null);
            }
            return conta;
        }
    }
}

