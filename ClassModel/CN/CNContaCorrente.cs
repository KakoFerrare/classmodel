﻿using ClassModel.DAO;
using ClassModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassModel.CN
{
    public class CNContaCorrente
    {
        DAOContaCorrente DaoContaCorrente = new DAOContaCorrente();
        public ContaCorrente Carregar(string id, string stringConnection)
        {
            return this.DaoContaCorrente.Carregar(id, stringConnection);
        }

        public List<ContaCorrente> Listar(string stringConnection)
        {
            return this.DaoContaCorrente.Listar(stringConnection);
        }

    }
}